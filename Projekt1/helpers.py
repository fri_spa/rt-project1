import csv
import collections
import numpy as np
import vietoris_rips as vr


def read_data(file_name, attributs_limit=0):
    header = []
    data = []
    first_row = True
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            if first_row:
                if attributs_limit == 0:
                    attributs_limit = len(row)
                header = row
                first_row = False
            else:
                row = row[0:attributs_limit]
                data.append(row)
    return data, header


def euler_characteristic(simplices):
    vertices, edges = vr.get_simplices_count_by_dim(simplices)
    return vertices - edges


def classify_components(components, iris_data=None):
    class_count = 1
    classes = []
    for c in components:
        indexes = []
        for x in c:
            while x:
                indexes.append(x.pop())
        indexes = np.unique(indexes)
        if iris_data:
            iris_set = []
            for ex in indexes:
                iris_set.append(iris_data[ex][4])

            members = collections.Counter(iris_set)
            classes.append([class_count, members.items()])
        else:
            classes.append([class_count, len(indexes)])
        class_count += 1
    return classes


def find_r_naive(data, num_components, max=2, min=0, radij = 0):

    for r in np.arange(max, min, -0.1):
        simplices = vr.build_vr(1, r, data)

        all_simplices = vr.get_all_simplices(simplices)
        components = vr.get_components(all_simplices)

        if len(components) > num_components:
            while len(components) != num_components:
                simplices = vr.build_vr(1, r, data)
                all_simplices = vr.get_all_simplices(simplices)
                components = vr.get_components(all_simplices)
                radij = r
                r += 0.01
            break
    return radij


def find_r(data, num_components, min=0, max=2.5, tol=0.01, dist_method="euclidean_distance"):
    if abs(max - min) <= tol:
        return float("{0:.2f}".format(max))

    r = float(min + max) / 2

    simplices = vr.build_vr(1, r, data, dist_method)

    all_simplices = vr.get_all_simplices(simplices)
    components = vr.get_components(all_simplices)

    if len(components) <= num_components:
        return float("{0:.2f}".format(find_r(data, num_components, min, r)))
    else:
        return float("{0:.2f}".format(find_r(data, num_components, r, max)))


def interval_of_r_value(data, components):
    r1 = find_r(data, components-1)
    r2 = find_r(data, components)
    return r1 - r2, r1, r2


def identify_number_of_classes(data):
    classes = 1
    longest_interval = 0
    for i in range(2, 7):
        current_interval, _, _ = interval_of_r_value(data, i)
        if current_interval > longest_interval:
            classes = i
            longest_interval = current_interval
    return classes
