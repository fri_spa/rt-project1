import random

class Simplex:
    def __init__(self, points_list, color=None):
        self.points = tuple(sorted(points_list, key=lambda point: point.point_number))
        self.level = len(points_list) - 1
        self.set_color(color)

    @property
    def dimension(self):
        return len(self.points)-1

    def __str__(self):
        points_str = ""
        comma = ""
        for p in self.points:
            points_str += comma + str(p)
            comma = ", "
        return "SX"+str(self.level)+"{ "+points_str+" }"

    def __repr__(self):
        return self.__str__()

    def get_x_y_color_as_list(self):
        x = []
        y = []
        color = []
        for point in self.points:
            x.append(point.x)
            y.append(point.y)
            color.append(self.simplex_color)
        return x, y, color

    def set_color(self, color=None):
        if color:
            self.simplex_color = color
        else:
            rc = lambda: random.randint(0, 255)
            self.simplex_color = '#%02X%02X%02X' % (rc(),rc(),rc())

    def equals(self, sx):
        if self.level != sx.level:
            return False
        else:
            points1 = sorted(self.points, key=lambda point: point.x)
            points2 = sorted(sx.points, key=lambda point: point.x)
            for i in range(len(points1)):
                if points1[i].x != points2[i].x or points1[i].y != points2[i].y:
                    return False

            return True