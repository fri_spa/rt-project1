import numpy as np
from collections import defaultdict
from Simplex import Simplex
from Complex import Complex
import copy

#measure=lambda p1, p2: math.sqrt(sum([(p1[i]-p2[i])**2 for i in range(len(p1))]))
class Vietoris_Rips:
    def __init__(self, points, r=0.1, measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1)-np.array(p2), 2)))):
        if len(points) == 0:
            raise "No points"
        self.points = points
        self.r = r
        self.measure = measure

    def __call__(self, r=None, measure=None):
        self.r = r if r else self.r
        self.measure = measure if measure else self.measure
        return self._vietoris_rips()

    def _vietoris_rips(self):
        """ r - radius
            measure - distance measure function
                      function accepts two points, each represented as numpy array
                      it returns distance between points
                      deafult: euclidian distance
        """
        VR_complex = set(self.points)

        points_by_distance = defaultdict(list)
        connected_points = []

        for i in range(len(self.points)):
            point1 = self.points[i]
            for j in range(i+1, len(self.points)):
                point2 = self.points[j]
                distance = self.measure(point1.point_as_tuple(), point2.point_as_tuple())
                if distance <= 2*self.r:
                    points_by_distance[distance].append(set([point1, point2]))
                    connected_points.append(set([point1, point2]))
                    VR_complex.discard(point1)
                    VR_complex.discard(point2)

        temp = []
        for sx in VR_complex:
            tmp = set()
            tmp.add(sx)
            temp.append(tmp)
        #VR_complex = temp


        complex = defaultdict(list)
        for point in self.points:
            s = set()
            s.add(point)
            complex[0].append(s)
        complex[1] = copy.deepcopy(connected_points)

        for i in range(len(connected_points)):
            line1 = connected_points[i]
            for j in range(i+1, len(connected_points)):
                line2 = connected_points[j]
                if len(line1 & line2) == 1:
                    for k in range(j+1, len(connected_points)):
                        line3 = connected_points[k]
                        dim3 = line1 | line2 | line3
                        if len(dim3) == 3:
                            complex[2].append(dim3)

        complex_temp = defaultdict(list)
        for dim in complex:
            for simplex in complex[dim]:
                complex_temp[dim].append(set([point.point_number for point in simplex]))


        VR_complex2 = []

        VR_complex2_temp = []

        def connect(dim):
            for i in range(len(complex[dim])):
                chain = set()
                chain_temp = set()
                dim3_1 = complex[dim][i]

                #chain_temp.update([point.point_number for point in dim3_1])
                chain_temp.update(complex_temp[dim][i])

                chain.add(tuple(dim3_1))
                for j in range(i+1, len(complex[dim])):
                    dim3_2 = complex[dim][j]
                    dim3_2_tuple = tuple(dim3_2)
                    #point_nrs = set([point.point_number for point in dim3_2])
                    point_nrs = complex_temp[dim][j]
                    if len(chain_temp & point_nrs) > 0 and dim3_2_tuple not in chain:
                        chain.add(dim3_2_tuple)
                        chain_temp.update(point_nrs)

                for i, subcomplex in enumerate(VR_complex2_temp):
                    intersection = subcomplex & chain_temp
                    diff = subcomplex - chain_temp
                    if len(intersection) > 0:
                        VR_complex2[i].update(chain)
                        VR_complex2_temp[i].update(chain_temp)
                        break
                else:
                    VR_complex2.append(chain)
                    VR_complex2_temp.append(chain_temp)



        nr_dim = 3
        for i in range(nr_dim)[::-1]:
            connect(i)

        VR_complex = []
        checked = set()
        #merge
        for i, subcomplex1 in enumerate(VR_complex2_temp):
            if i not in checked:
                final_subcomplex = set()
                final_subcomplex_temp = set()
                final_subcomplex.update(VR_complex2[i])
                final_subcomplex_temp.update(subcomplex1)
                for j in range(i+1, len(VR_complex2_temp)):
                    subcomplex2 = VR_complex2_temp[j]
                    intersection = final_subcomplex_temp & subcomplex2
                    if len(intersection) > 0 and j not in checked:
                        checked.add(j)
                        final_subcomplex.update(VR_complex2[j])
                        final_subcomplex_temp.update(subcomplex2)
                if len(final_subcomplex)>0:
                    VR_complex.append(final_subcomplex)

        chains = []
        for chain in VR_complex:
            chain_s = [Simplex(tuple(simplex)) for simplex in chain]
            chains.append(chain_s)

        return Complex(chains, self.r)