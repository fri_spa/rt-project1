import Vietoris_Rips
import numpy as np


def find_r(data, measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1) - np.array(p2), 2))), n=1, min=0, max=1, tol=0.02):
    if (abs(max - min) <= tol):
        return float("{0:.2f}".format(max))

    radij = float(min + max) / 2
    VR_con_min = Vietoris_Rips.main(data, radij, measure)
    if (len(VR_con_min) <= n):
        return float("{0:.2f}".format(find_r(data, measure, n, min, radij)))
    else:
        return float("{0:.2f}".format(find_r(data, measure, n, radij, max)))


def find_max_interval(data, measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1) - np.array(p2), 2))), start=1,
                      stop=6):
    # med katerima kompleksoma je interval
    n, m = -1, -1
    radij_razlika = -1
    r1, r2 = 0, 0
    r_n = find_r(data, measure, start)
    r1 = r_n

    for i in range(start + 1, stop):
        r_m = find_r(data, measure, i, 0, r_n)

        temp_radij_razlika = abs(r_n - r_m)
        if temp_radij_razlika >= radij_razlika:
            radij_razlika = temp_radij_razlika
            n = i - 1
            m = i
            r1 = r_n
            r2 = r_m

        r_n = r_m

    return n, m, r1, r2  # ,radij_razlika
