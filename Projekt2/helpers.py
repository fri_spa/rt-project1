import csv

import numpy as np
import random


def read_data(file_name):
    data = []
    with open(file_name, 'rb') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            data.append(np.array(row[1:]).astype(float).tolist())
    return data


def f_range(start, end=None, inc=None):
    "A range function, that does accept float increments..."
    if end == None:
        end = start + 0.0
        start = 0.0
    if inc == None:
        inc = 1.0
    L = []
    while 1:
        next = start + len(L) * inc
        if inc > 0 and next > end:
            break
        elif inc < 0 and next < end:
            break
        L.append(next)
    return L


def shake(points, radius, shake_error=0.05):
    return map(lambda x: map(lambda y: y + (random.uniform(0, shake_error) * radius), x), points)
