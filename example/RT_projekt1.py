import csv
import math
import numpy as np
import random
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider
import Vietoris_Rips
import radius
from VRClassifier import *

def read_iris(file_name):
    reader = csv.reader(open(file_name), delimiter=",")
    data = []
    for row in reader:
        if len(row) > 0:

            line = [float(value) for value in row[:-1]]
            line.append(row[-1])
            data.append(line)
    return data

def read_servo(file_name):
    reader = csv.reader(open(file_name), delimiter=",")
    data = []
    replace = {'A': 0.1, 'B': 0.2, 'C': 0.6, 'D': 0.8, 'E': 0.9}
    #replace = {'A': 0.1, 'B': 0.2, 'C': 0.3, 'D': 0.4, 'E': 0.5}
    for row in reader:
        if len(row) > 0:
            row[0] = replace[row[0]]
            row[1] = replace[row[1]]
            line = [float(value) for value in row[:-1]]
            line.append(float(row[-1]))
            data.append(line)
    return data


def plot(data, testVR, r=0, offset=4, multiple_plots=False, d1=0, d2=1):
    x = [point[0] for point in data]
    y = [point[1] for point in data]

    l, = plt.plot(x, y, 'r.')

    plt.axis([min(x)-offset, max(x)+offset, min(y)-offset, max(y)+offset])
    rc = lambda: random.randint(0,255)
    colors = ['#%02X%02X%02X' % (rc(),rc(),rc()) for _ in range(len(x))]
    circles = []
    for i, point in enumerate(data):
        circle = plt.Circle(point, r, color=colors[i], clip_on=False, fill=True, alpha=0.8)
        circles.append(circle)
        plt.gcf().gca().add_artist(circle)

    plt.axes().set_aspect('equal', 'datalim')
    plt.grid()
    axcolor = 'lightgoldenrodyellow'
    axradius = plt.axes([0.19, 0.02, 0.65, 0.03], axisbg=axcolor)
    sradius = Slider(axradius, 'radij', 0.0, 3, valinit=r)



    def update(val):
        global vr_classifier
        VR_complex=testVR(val)
        VR_complex_temp = [set([(point[d1], point[d2]) for point in complex]) for complex in VR_complex]
        group_colors = [colors[i] if i<len(colors) else '#%02X%02X%02X' % (rc(),rc(),rc()) for i in range(len(VR_complex))]
        for i, circle in enumerate(circles):
            circle.set_radius(val)
            for j in range(len(VR_complex_temp)):
                if circle.center in VR_complex_temp[j]:
                    color = group_colors[j]
                    circle.set_fc(color)
                    circle.set_ec(color)
                    break

        score = vr_classifier.estimate_classification_score(VR_complex)
        print("Classification score: %f" % score)
        print("")
        text = "Stevilo skupin: "+ str(len(VR_complex))+",  p-norm:"+str(p) + "\n"+"Klasifikacijska tocnost: "+str(round(score, 5))
        plt.title(text, y=30)
        plt.draw()

    sradius.on_changed(update)
    update(r)
    if not multiple_plots:
        plt.show()




def plot_test():
    #test_data = [(0, 1), (1, 1), (2, 1), (1, 2.3), (1, 3.5), (1, 4.5), (1, 5)]
    test_data = [(0, 0), (1, 1), (1, 1+math.sqrt(2))]
    #test_data = [(0, 0), (1, math.sqrt(2**2-1**2)), (2, 0)]
    n=10
    #test_data = [(random.uniform(0,10), random.uniform(0,10)) for _ in range(n)]
    radius = 0.1

    #manhatten
    measure = lambda p1, p2: np.sum(np.abs(np.array(p1)-np.array(p2)))
    #euclidian
    measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1)-np.array(p2), 2)))
    def testVR(r):
        VR_conplex = Vietoris_Rips.main(test_data, r, measure)
        #print(VR_conplex)
        print("Stevilo skupin:", len(VR_conplex), ", radij: ", r)
        return VR_conplex
    testVR(radius)
    plot(test_data, testVR, radius)

p=2
def data_test(data):
    data = [tuple(d[:-1]) for d in data]
    
    #hamming distance
    #measure = lambda s1, s2: sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))
    #manhatten
    #measure = lambda p1, p2: np.sum(np.abs(np.array(p1)-np.array(p2)))
    #euclidian
    #measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1)-np.array(p2), 2)))
    #p-norm
    measure = lambda p1, p2: np.power(np.sum(np.power(np.abs(np.array(p1)-np.array(p2)), p)), 1./p)

    num_component = 3

    r = radius.find_r( data, measure, num_component)
    
    m,n,r1,r2 = radius.find_max_interval(data, measure)
    print("Naravni radij je na intrvalu od" ,r2," do",r1)
    
    def testVR(r):
        VR_conplex = Vietoris_Rips.main(data, r, measure)
        print("Stevilo skupin:", len(VR_conplex), ", radij: ", r)
        return VR_conplex
    testVR(r)

    plt.figure(1)
    plot([(d[0], d[1]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=0, d2=1, )
    plt.figure(2)
    plot([(d[0], d[2]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=0, d2=2, )
    plt.figure(3)
    plot([(d[0], d[3]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=0, d2=3, )
    plt.figure(4)
    plot([(d[1], d[2]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=1, d2=2, )
    plt.figure(5)
    plot([(d[1], d[3]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=1, d2=3, )
    plt.figure(6)
    plot([(d[2], d[3]) for d in data], testVR, r, offset=0.5, multiple_plots=True, d1=2, d2=3,)
    plt.show()



if __name__ == "__main__":
    # random.seed(42)
    data = read_iris("iris.txt")
    #data = read_servo("servo.txt")
    vr_classifier = VRClassifier(data)
    data_test(data)