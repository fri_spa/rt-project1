

class Filtrator:
    def __init__(self, complex_states_list, debug = True):
        self.complexes_count = len(complex_states_list)
        self.complexes = complex_states_list
        self.previous_state = None
        self.current_state = None
        self.debug = debug
        self.states = []
        self.object_counter = 0


    def persistent_homology_filtration(self):
        latest_state = self.analyze_first_state()
        self.states.append(latest_state)
        for i in range(self.complexes_count-1):
            latest_state = self.filter_changes(latest_state, self.complexes[i+1], i+1)
            self.states.append(latest_state)

        return self.states

    # Create the first state from the first chain list
    def analyze_first_state(self):
        if self.debug:
            print("")
            print("STEP 0")
            print("-------------------------------")
        complex = self.complexes[0]
        state = FiltrationState(0, complex)

        # Every chain is a new object, so just create it and save it
        for chain in complex.chains_by_dim:
            name = ("OBJ-%3d" % self.object_counter)
            object = FiltrationObject(name, chain, 0)
            state.add_newborn_object(object)
            self.object_counter+=1

            if self.debug:
                print("New object born: "+name+" - "+ str(chain))

        return state


    def filter_changes(self, previous_state, complex, step):
        if self.debug:
            print("")
            print("STEP "+str(step))
            print("-------------------------------")

        state = FiltrationState(step, complex)
        prev_complex = previous_state.raw_complex # TODO: do you need it?

        # GO through all new chains
        for chain in complex.chains_by_dim:
            # For each 0-dim simplex of this chain try to find out in which object it was in the previous state
            # This is a bit simplified, because we know that 0-dim simplices will always be the same as the points won't change ever
            # They will, however, get connected to different chains as the radius is increased
            zero_dim_simplices = chain[0]
            prev_state_object_names = [ previous_state.find_object_name(sx) for sx in zero_dim_simplices ]

            # TODO: Check for Nones - new points (not likely)

            # TODO: Check (first?) if some chains were broken up - by reverse comparison

            # One possible outcome is that all these simplices belong to the same object
            # In this case we will just check if any of the higher level simplices were added
            # But other than that, this chain contains the same points as in previous state
            distinct_object_names = list(set(prev_state_object_names))
            if len(distinct_object_names) == 1:
                # TODO: check for updates

                # If nothing is updated just add this object back to state
                prev_object = previous_state.get_object_by_name(distinct_object_names[0])
                new_object = FiltrationObject(prev_object.name, chain, prev_object.birth_state)
                state.keep_state_object(new_object)
                if self.debug:
                    print("Object unchanged:"+prev_object.name+" - "+ str(chain))

            # The other outcome is that in the previous state, this chain was segmented among multiple objects
            # Now we have a list which they were
            # So we will try to find the oldest object of all those and merge it (ie. keep it with the new chain)
            # All the other objects will be marked as dead
            else:
                objects = [ previous_state.get_object_by_name(on) for on in distinct_object_names ]
                objects.sort(key=lambda o: o.birth_state)
                # The first one will be kept
                new_object = FiltrationObject(objects[0].name, chain, objects[0].birth_state)
                state.update_state_object(new_object)
                if self.debug:
                    print("Object survived: "+new_object.name+" - "+ str(chain))
                # And others are dead
                for i in range(1, len(objects), 1):
                    state.add_dead_object(objects[i])
                    if self.debug:
                        print("Object died: "+objects[i].name+" - "+ str(objects[i].chain))

        return state

class FiltrationState:
    def __init__(self, step_number, raw_complex):
        self.step_number = step_number
        self.raw_complex = raw_complex
        self.newborn_objects = []
        self.dead_objects = []
        self.updated_objects = []
        self.all_state_objects = []

    # Add newborn object: save it in the list of newborns in this state as well as in all objects of this state
    def add_newborn_object(self, obj):
        self.newborn_objects.append(obj)
        self.all_state_objects.append(obj)

    # Add object that has died in this state, this object will not be included in all objects of this state
    def add_dead_object(self, obj):
        self.dead_objects.append(obj)

    # Update state object, for those objects that live through this iteration but are at least slightly changed
    def update_state_object(self, obj):
        self.all_state_objects.append(obj)
        self.updated_objects.append(obj)

    # State object that remained constant through this state
    def keep_state_object(self, obj):
        self.all_state_objects.append(obj)

    # Find the object name of given simplex in this state
    def find_object_name(self, simplex):
        for obj in self.all_state_objects:
            if obj.contains(simplex):
                return obj.name

        return None

    # Get object by it's name
    def get_object_by_name(self, name):
        for obj in self.all_state_objects:
            if obj.name == name:
                return obj

        return None




class FiltrationObject:
    def __init__(self, name, chain, birth_state):
        self.name = name
        self.chain = chain
        self.birth_state = birth_state

    def contains(self, simplex):
        if simplex.level not in self.chain.keys():
            return False
        else:
            for sx in self.chain[simplex.level]:
                if sx.equals(simplex):
                    return True


