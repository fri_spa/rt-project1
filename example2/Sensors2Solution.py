from TestDataReader import TestDataReader
from Simplex import Simplex
from Point import Point
from matplotlib import pyplot as plt
import PlotPersistence
from Vietoris_Rips import Vietoris_Rips
import random
from Cech import Cech
import datetime
from Filtrator import Filtrator

def shake_points(points, shake_magnitude=0.1):
    new_points = []
    for point in points:
        new_points.append(Point.shake(point, shake_magnitude=shake_magnitude))
    return new_points

def plot_complex(complex, r=0.05, step=0.05, nr_iterations=9, figure_nr_start=0):
    for i in range(nr_iterations):
        VR_Complex = complex(r + (i)*step)
        VR_Complex.plot(figure_nr=figure_nr_start+i, circles=True, annoatations=False, triangles=True)


def test_bar_code():
    points = [(0, 1), (0.3, 1.5), (0.8, 1.2), (2.3, 4.5), (1.5, 3.0), (1, 8), (4,7), (3,5.6), (5,8)]
    PlotPersistence.bar_code(points)
def test_persistence_diagram():
    points = [(0, 1), (0.3, 1.5), (0.8, 1.2), (2.3, 4.5), (1.5, 3.0), (1, 8), (4,7), (3,5.6), (5,8)]
    PlotPersistence.persistence_diagram(points)




if __name__ == "__main__":
    test_points = TestDataReader.get_test_data(1)
    #test_points = [Point(0, 0, 0), Point(3, 0, 1), Point(1.5, 2.598, 2)]
    vr = Vietoris_Rips(test_points)

#    results = TestDataReader.get_results_from_file("data/s1_150_3_1.6_4.txt")
#    PlotPersistence.plot_bar_code(results)

    start = datetime.datetime.now()
    vr_res1 = vr(r=1.5)
 #   vr_res2 = vr(r=0.4)
    #vr_res2 = vr(r=0.30)
    #vr_res3 = vr(r=0.37)
    end = datetime.datetime.now()
    print(end-start)

    #vr_res1 = vr(r=1.06/2)
    #cz = Cech(test_points, r=0.1)
    #cz_res1 = cz(r=0.15)
    #cz_res2 = cz(r=0.37)

    #filtrator = Filtrator([vr_res1, vr_res2, vr_res3], debug=True)
    #homology = filtrator.persistent_homology_filtration()


#    vr_res1.plot(figure_nr=1)
#    vr_res2.plot(figure_nr=2)
#    print(len(vr_res1.chains))
#    print(len(vr_res2.chains))
    #vr_res2.plot(figure_nr=2)
    #vr_res3.plot(figure_nr=3)
    #cz_res1.plot(figure_nr=11)
    #cz_res2.plot(figure_nr=12)

    #print(len(vr_res1.chains))
    #print((vr_res1.chains))
    #print(cz_res1)

    #shaked_points = shake_points(test_points, shake_magnitude=0.3)
    #plot_complex(construct_VR(shaked_points), step=0.3, nr_iterations=1, figure_nr_start=10)
    plot_complex(vr, r=0.5, step=0.5, nr_iterations=4, figure_nr_start=20)

    #test_bar_code()
    #test_persistence_diagram()

    plt.show()
