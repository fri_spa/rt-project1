from matplotlib import pyplot as plt

def bar_code(points,dim, fig_nr=0, offset=0.5):
    """
    :param points: list of tuples->(birth, death)
    :return:
    """
    fig = plt.figure(fig_nr)

    x = []
    y = []

    for i, point in enumerate(points):
        x += list(point)
        y += [i, i]

    plt.plot(x, y, "ro")

    for i in range(0, len(x), 2):
        line = plt.Line2D([x[i], x[i+1]], [y[i], y[i+1]], lw=1.5, color="r")
        plt.gca().add_line(line)
    print(max(x))
    plt.axis([min(x)-offset, max(x), min(y)-offset, max(y)+offset])
    #plt.axes().set_aspect('equal', 'datalim')
    plt.grid()
    text = "Dimension: "+str(dim)
    fig.suptitle(text, fontsize=20)


def persistence_diagram(points, dim, fig_nr=0, offset=0.5):
    fig = plt.figure(fig_nr)

    x = []
    y = []

    for i, point in enumerate(points):
        x.append(point[0][0])
        y.append(point[0][1])

    plt.axis([0, max(max(y), max(x))+offset, 0, max(max(y), max(x))+offset])
    plt.axes().set_aspect('equal', 'datalim')
    plt.grid()

    line = plt.Line2D([0, max(max(y), max(x))+offset], [0, max(max(y), max(x))+offset], lw=1.5, color="g")
    plt.gca().add_line(line)

    plt.plot(x, y, "ro")

    for i in range(len(x)):
        plt.annotate(points[i][1], (x[i]+0.1, y[i]+0.1))

    text = "Dimension: "+str(dim)
    fig.suptitle(text, fontsize=20)


def plot_bar_code(results):
    for dim in results:
        r = []
        for result, nr in results[dim]:
            for i in range(nr):
                r.append(result)
        bar_code(r, dim, fig_nr=dim)
        persistence_diagram(results[dim], dim, fig_nr=dim+5)
