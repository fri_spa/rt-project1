from math import sqrt


def euclidean_distance(v):
    return sqrt(sum((pow(x, 2) for x in v)))


def manhattan_distance(v):
    return sqrt(sum((abs(x) for x in v)))


def p_norm_distance(v, p=2):
    return pow(sum((pow(x, p) for x in v)), 1.0 / p)


# Pairwise distances between the elements of `points` with respect to some `norm`
class PairwiseDistancesRT:
    def __init__(self, points, norm=euclidean_distance):
        self.points = points
        if norm == 'manhattan_distance' or norm == 'manhattan':
            self.norm = manhattan_distance
        elif norm == 'p_norm_distance' or norm == 'p_norm':
            self.norm = p_norm_distance
        else:
            self.norm = euclidean_distance

    def __len__(self):
        return len(self.points)

    def __call__(self, p1, p2):
        return self.norm((x - y for (x, y) in zip(self.points[p1], self.points[p2])))
