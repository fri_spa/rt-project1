from matplotlib import pyplot as plt
import random
from collections import defaultdict
from Simplex import Simplex
import math
class Complex:
    def __init__(self, chain_of_simplices, r=0.5):
        self.chains = chain_of_simplices #type of list
        self.r = r
        self.chains_by_dim = []
        for chain in self.chains:
            chain_by_dim = defaultdict(list)
            color = self._color()
            for simplex in chain:
                simplex.set_color(color)
                chain_by_dim[simplex.dimension].append(simplex)
            self.chains_by_dim.append(chain_by_dim)

    def get_nr_of_chains(self):
        return len(self.chains_by_dim)

    def get_chain_nr(self, nr):
        return self.chains[nr]

    def _color(self):
        rc = lambda: random.randint(0, 255)
        return '#%02X%02X%02X' % (rc(),rc(),rc())

    def __str__(self):
        s = ""
        for chain in self.chains_by_dim:
            for dim in chain:
                s += str(dim) + ": " + str(chain[dim])+"\n"
            s+="\n"
        return s

    def __repr__(self):
        return self.__str__()

    def plot(self, offset=0.5, figure_nr=1, dots=True, lines=True, triangles=True, circles=True, annoatations=True):
        fig = plt.figure(figure_nr)
        x = []
        y = []
        colors = []

        for chain in self.chains_by_dim:
            for simplex in chain[0]:
                x_, y_, color = simplex.get_x_y_color_as_list()
                x += x_
                y += y_
                colors += color

        if circles:
            circles = []
            for i in range(len(x)):
                circle = plt.Circle((x[i], y[i]), self.r, color=colors[i], clip_on=False, fill=True, alpha=0.7)
                circles.append(circle)
                plt.gca().add_patch(circle)

        if dots:
            plt.plot(x, y, "r.")

        for chain in self.chains_by_dim:
            if lines:
                for simplex in chain[1]:
                    x_, y_, color = simplex.get_x_y_color_as_list()
                    line = plt.Line2D(x_, y_, lw=1.5, color="r")
                    plt.gca().add_line(line)

            if triangles:
                for simplex in chain[2]:
                    x_, y_, color = simplex.get_x_y_color_as_list()
                    coords = list(zip(x_, y_))
                    triangle = plt.Polygon(coords, alpha=0.6)
                    plt.gca().add_patch(triangle)

        if annoatations:
            i = 0
            for chain in self.chains_by_dim:
                for simplex in chain[0]:
                    plt.annotate(simplex.points[0].point_number, (x[i], y[i]))
                    i += 1

        plt.axis([min(x)-offset, max(x)+offset, min(y)-offset, max(y)+offset])



        plt.axes().set_aspect('equal', 'datalim')
        plt.grid()
        #plt.title("Test", y=30)
        #plt.draw()
        text = "Vietoris-Rips, r="+str(round(self.r*100)/100)
        fig.suptitle(text, fontsize=20)
        #axcolor = 'lightgoldenrodyellow'
        #axradius = plt.axes([0.19, 0.02, 0.65, 0.03], axisbg=axcolor)
        #sradius = Slider(axradius, 'radij', 0.0, 3, valinit=r)
