import csv
import math
import numpy as np
import random

class VRClassifier:

    def __init__(self, data):
        self.data = np.asarray([np.asarray(a) for a in data])
        self.previous_complex_count = -1
        self.previous_score = -1
        self.analyze_data_classes(self.data)
        self.max_classes_limit = 20


    def analyze_data_classes(self, data):
        self.classes = list(set(data[:, -1]))
        self.number_of_classes = len(self.classes)




    def estimate_classification_score(self, complex):

        if len(complex) != self.previous_complex_count and len(complex) < self.max_classes_limit:
            score = self.find_best_classification_score(complex)
            self.previous_score = score
            return score


        return self.previous_score



    def find_best_classification_score(self, complex):
        complex_class_distributions = []

        for c in complex:
            dist = self.find_complex_class_distribution(c)
            complex_class_distributions.append(dist)

        correct, incorrect = self.fit_classes(complex_class_distributions)

        return float(correct)/(correct+incorrect)


    def find_complex_class_distribution(self, complex):
        dist = {}
        for cls in self.classes:
            dist[cls] = 0

        for c in complex:
            cls = self.find_class_of_example(c)
            dist[cls] += 1

        return dist

    def find_class_of_example(self, example):
        l = len(example)

        filtered_rows = self.data
        for i in range(l):
            example_property = str(example[i])
            filtered_rows = filtered_rows[(filtered_rows[:,i] == example_property)]

        if len(filtered_rows) < 1:
            print("Unable to match the example:")
            print(example)
            return "unmatched"
        elif len(filtered_rows) > 1:
            print("Ambiguously matched example:")
            print(example)

        return filtered_rows[0, -1]


    def fit_classes(self, distributions):

        correct = 0
        incorrect = 0

        available_classes = self.classes[:]
        unavailable_classes = []

        def keywithmaxval(d):
            r = dict(d)
            for c in unavailable_classes:
                del r[c]
            v=list(r.values())
            k=list(r.keys())
            return k[v.index(max(v))]


        def max_class_best_score_comparer(dist):
            incorrects = 0
            largest_group = keywithmaxval(dist)

            for k, v in dist.items():
                if k != largest_group:
                    incorrects += v

            return dist[largest_group] - incorrects

        while len(available_classes) > 0 and len(distributions) > 0:
            distributions = sorted(distributions, key=max_class_best_score_comparer)
            dist = distributions.pop()
            cls = keywithmaxval(dist)

            available_classes.remove(cls)
            unavailable_classes.append(cls)

            for k, v in dist.items():
                if k == cls:
                    correct += v
                else:
                    incorrect += v

        for dist in distributions:
            for k, v in dist.items():
                incorrect += v

        return correct, incorrect