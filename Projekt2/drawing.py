from matplotlib import pyplot as plt
from matplotlib.patches import Polygon
import numpy as np


def draw_complex(points, simplices, radius, figTitle="Complex"):
    pts = []
    lines = []
    triangles = []

    for sx in simplices:
        v = list(sx.vertices)
        sx_points = get_points_by_indices(points, v)
        if len(sx_points) == 1:
            pts.append(sx_points)
        elif len(sx_points) == 2:
            lines.append(sx_points)
        elif len(sx_points) == 3:
            triangles.append(sx_points)

    plt.figure()
    for sx in pts:
        draw_point(sx, radius)
    for sx in lines:
        draw_line(sx)
    for sx in triangles:
        draw_triangle(sx)

    plt.title(figTitle + " (r = " + str(radius) + ")", fontsize=12, weight='bold')
    plt.show()


def get_points_by_indices(points, indices):
    """
    Get data from point array on the given indices.
    Useful since simplex spanned by a list of points
    is given as a list of positions of the points
    in a points array.
    """
    return [points[index] for index in indices]


def draw_triangle(points):
    """
    Draw a triangle on the current figure.
    Triangle must be given as a list of three 2D points,
    each point as a list of two numbers.
    """
    if len(points) != 3:
        raise Exception("draw_triangle() expects three points")
    p1, p2, p3 = points
    # plt.plot([p1[0], p2[0]], [p1[1], p2[1]])
    # plt.plot([p1[0], p3[0]], [p1[1], p3[1]])
    # plt.plot([p2[0], p3[0]], [p2[1], p3[1]])

    polygon = Polygon(np.array([p1, p2, p3]), True, color='red', alpha=0.2)
    fig = plt.gcf()
    fig.gca().add_artist(polygon)
    # fig.canvas.draw()


def draw_line(points):
    """
    Draw a line on the current figure.
    Line must be given as a list of two 2D points,
    each point as a list of two numbers.
    """
    if len(points) != 2:
        raise Exception("draw_line() expects two points")
    p1, p2 = points
    plt.plot([p1[0], p2[0]], [p1[1], p2[1]], color='black')


def draw_point(point, radius):
    """
    Draw a point on the current figure.
    Point must be given as a list of two numbers.
    """
    if len(point) != 1:
        raise Exception("draw_point() expects one point")

    point = point[0]
    plt.plot(point[0], point[1], 'ro')
    circle = plt.Circle(point, radius, color='b', alpha=0.1)
    fig = plt.gcf()
    fig.gca().add_artist(circle)
    fig.gca().set_aspect('equal', adjustable='box')
    # fig.canvas.draw()


def draw_barcode_diagram(points, title, radius, inf_factor):
    """
    Draws a barcode diagram from list of point provided as input parameter.
    Also draws a radius, represented as blue vertical dashed line.
    """
    dim = len(points)
    offset = 0.5
    plt.figure()
    for i in range(0, dim):
        point = points[i]
        if dim == 2:
            ax = plt.subplot(2, 1, i + 1)
            ax.set_title("Dimension" + str(i), fontsize=11)
        else:
            plt.subplot(3, 1, i + 1)

        # draw birth, death points
        x, y = [], []
        for i, p in enumerate(point):
            x += list(p)
            y += [i, i]

        plt.plot(x, y, "ro")

        # draw lines
        for i in range(0, len(x), 2):
            line = plt.Line2D([x[i], x[i + 1]], [y[i], y[i + 1]], lw=1.5, color="r")
            plt.gca().add_line(line)
        plt.axis([min(x) - offset, max(x), min(y) - offset, max(y) + offset])
        plt.xlim(-0.5, inf_factor)
        plt.axvline(x=radius, ls='dashed')
        plt.grid()
    plt.suptitle(title, fontsize=12, weight='bold')
    plt.show()
