from Point import Point
import sys
from collections import defaultdict
from operator import itemgetter

class TestDataReader:

    @staticmethod
    def get_test_data(number):
        return TestDataReader.get_test_data_from_file("data/s"+str(number)+".out")

    @staticmethod
    def get_test_data_from_file(filename):
        points = []
        i = 0
        with open(filename) as f:
            for line in f:
                if line == '\n':
                    break
                line_parts = line.strip().split(' ')
                point = Point(line_parts[0], line_parts[1], i)
                i += 1
                points.append(point)

        return points


    @staticmethod
    def get_results_from_file(filename):
        data_by_dim = defaultdict(list)
        f = open(filename)
        cur_dim = 0
        for line in f:
            if line.strip() == "":
                break
            l = line.strip().split(" ")
            if l[0] == "Dimension:":
                cur_dim = int(l[1])
            else:
                birth = float(l[0][1:-1])
                death = l[1][0:-1]
                death = 10 if death == "infinity" else float(death)
                data_by_dim[cur_dim].append(tuple([birth, death]))

        #for dim in data_by_dim:
           # l = data_by_dim[dim]
           # m = max(max(l, key=itemgetter(1)))

           # for i, el in enumerate(data_by_dim[dim]):
           #     if el[1] == -1:
           #         data_by_dim[dim][i][1] = m*5


        data_by_dim_with_nr = defaultdict(list)
        d = defaultdict(list)
        for dim in data_by_dim:
            d[dim].append(set(data_by_dim[dim]))
        for dim in d:
            for s in d[dim]:
                for t in s:
                    nr = 0
                    for i, tt in enumerate(data_by_dim[dim]):
                        if t==tt:
                            nr += 1
                    data_by_dim_with_nr[dim].append(tuple((t, nr)))

        return data_by_dim_with_nr

