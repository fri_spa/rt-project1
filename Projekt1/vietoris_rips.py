from dionysus import Rips, PairwiseDistances, Filtration
import distance as d


# Rips object attrs: 'cmp', 'edge_cofaces', 'eval', 'generate', 'vertex_cofaces'
# Simplex object attrs: 'add', 'boundary', 'contains', 'data', 'dimension', 'join', 'vertices'


def build_vr(max_dim, r, points, dist_method='p_norm_distance'):
    dist = d.PairwiseDistancesRT(points, dist_method)
    rips = Rips(dist)
    simplices = Filtration()
    rips.generate(max_dim, r, simplices.append)
    return simplices


def get_simplices_count_by_dim(simplices):
    dim0 = 0
    dim1 = 0
    for s in simplices:
        if hasattr(s, 'dimension'):
            if s.dimension() == 0:
                dim0 += 1
            if s.dimension() == 1:
                dim1 += 1
        else:
            if len(s) == 1:
                dim0 += 1
            if len(s) == 2:
                dim1 += 1
    return dim0, dim1


def get_simplices(dim, simplices):
    scs = []
    for s in simplices:
        sx = []
        for v in s.vertices:
            sx.append(v)
        if len(sx) == dim+1:
            scs.append(sx)
    return scs


def get_all_simplices(simplices):
    scs = []
    for s in simplices:
        sx = []
        for v in s.vertices:
            sx.append(v)
        scs.append(sx)
    return scs


def get_components(simplices):
    simplices = list(simplices)
    cmps = []
    while len(simplices) > 0:
        sx = simplices.pop(0)
        comp = get_component(sx, simplices)
        comp.insert(0, sx)
        cmps.append(comp)
    return cmps


# 1. find all simplices connected to sx
# 2. remove them from the list
# 3. recursively check for further connections
def get_component(sx, simplices):
    connected = []
    v1 = sx[0]
    connected.extend(get_connected_simplices(v1, simplices))
    if len(sx) == 2:  #edge
        v2 = sx[1]
        connected.extend(get_connected_simplices(v2, simplices))

    for s in connected:
        simplices.remove(s)

    c = len(connected)
    for i in range(0, c):
        s = connected[i]
        connected.extend(get_component(s, simplices))

    return connected


def get_connected_simplices(v, simplices):
    connected = []
    for s in simplices:
        if len(s) == 1 and (s[0] == v) \
                or len(s) == 2 and (s[0] == v or s[1] == v):
            connected.append(s)
    return connected
