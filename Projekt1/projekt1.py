# coding=utf-8
import vietoris_rips as vr
import helpers as h
import numpy as np
from matplotlib import pyplot as plt
import copy


def main():
    dist_method = 'p_norm_distance'  # euclidean_distance

    # # read iris data
    # iris_data, iris_header = h.read_data("data/iris.data")
    # test_data(iris_data, iris_header, "Iris", dist_method)

    # # read triangulated_monkey data
    # monkey_data, monkey_header = h.read_data("data/triangulated_monkey.data")
    # test_data(monkey_data, "Triangulated monkey", dist_method)

    # read  torus_around_cone data
    torus_data, torus_header = h.read_data("data/torus_around_cone.data", 3)
    test_data(torus_data, torus_header, "Torus around cone", dist_method)


def test_data(data, header, name="other", dist_method="p_norm_distance"):
    print ("Testing "+name+" dataset\n").upper()
    if name.lower() == "iris":
        points = [np.array(p[0:-1]).astype(float).tolist() for p in data]
    else:
        points = [np.array(p).astype(float).tolist() for p in data]
    points_copy = copy.deepcopy(points)
    weights = [20, 15, 5, 25, 20]
    # weights = [1.4, 0.5, 5.0, 5.0]
    if name.lower() == "iris" and len(weights) != len(np.array(data[0][0:-1])):
        print "Weights dimension doesn't match with data dimension! Default weights (ones) will be used."
        weights = np.ones(len(np.array(data[0][0:-1])))
    elif name.lower() != "iris" and len(weights) != len(np.array(data[0])):
        print "Weights dimension doesn't match with data dimension! Default weights (ones) will be used."
        weights = np.ones(len(np.array(data[0])))

    print "Identifying natural number of classes... be patient"
    # identify number of classes
    classes = h.identify_number_of_classes(points_copy)
    print "Identified number of classes: ", classes

    # for iris we choose 3 components
    if name.lower() == "iris":
        classes = 3

    # calculate r1
    r1 = h.find_r(points, 1, dist_method=dist_method)    # 1.64
    print "r1: ", r1

    # calculate interval of r value
    interval, r1, r2 = h.interval_of_r_value(points, classes)     # 0.74 and 0.82
    print "Interval of r for", classes, " components is from ", r2, " to", r1

    # build VR with value r such that we have 3 components (=number of classes)
    max_sx_dim = int(1)
    r = (r1 + r2) / 2
    # r = 0.78  # for Iris and p-norm
    simplices = vr.build_vr(max_sx_dim, r, points, dist_method)
    all_simplices = vr.get_all_simplices(simplices)
    components = vr.get_components(all_simplices)
    components_copy = copy.deepcopy(components)

    # assign colors to points (representing component)
    all_colors = ['r', 'b', 'c', 'g', 'm', 'y', 'b']
    colors = all_colors[:len(components)]
    for i in range(0, len(components)):
        comp = components[i]
        for sx in comp:
            p1_ix = sx[0]
            p1 = points[p1_ix]
            if not colors[i] in p1:
                p1.append(colors[i])
                points[p1_ix] = p1
            if len(sx) == 2:
                p2 = sx[1]
                p2_ix = sx[1]
                p2 = points[p2_ix]
                if not colors[i] in p2:
                    p2.append(colors[i])
                    points[p2_ix] = p2

    # plot components
    radius = float(r1)/2
    attrs_count = len(points[0]) - 1
    fig_num = 1
    for x in range(0, attrs_count):
        for y in range(x + 1, attrs_count):
            plt.figure(fig_num)
            min_x = float("inf")
            max_x = -float("inf")
            min_y = float("inf")
            max_y = -float("inf")
            for point in points:
                xcoord = point[x]
                ycoord = point[y]
                color = point[-1]
                if xcoord < min_x:
                    min_x = xcoord
                if xcoord > max_x:
                    max_x = xcoord
                if ycoord < min_y:
                    min_y = ycoord
                if ycoord > max_y:
                    max_y = ycoord
                plt.plot(xcoord, ycoord, color+'.')
                circle = plt.Circle((xcoord, ycoord), radius, color=color, alpha=0.01)
                fig = plt.gcf()
                fig.gca().add_artist(circle)
                fig.gca().set_aspect('equal', adjustable='box')
            plt.xlabel(header[x], style='italic')
            plt.ylabel(header[y], style='italic')
            plt.title(name+" dataset", fontweight='bold')
            plt.xlim(min_x - radius, max_x + radius)
            plt.ylim(min_y - radius, max_y + radius)
            fig_num += 1

    # classification
    # Iris dataset
    if name.lower() == "iris":
        classes = h.classify_components(components, data)
        print 'Classification\n\tWe get %d %s:' % (len(components), 'classes' if len(components) > 1 else 'class')
        for cls_nr, names in classes:
            print '\t* Class %d has %d distinct %s from original dataset: %s' % (
                cls_nr, len(names), 'samples' if len(names) > 1 else 'sample', names)

    # Other datasets
    else:
        classes = h.classify_components(components)
        print 'Classification\n\tWe get %d %s:' % (len(components), 'classes' if len(components) > 1 else 'class')
        for c, cls_nr in classes:
            print '\t* Class %d has %d %s' % (c, cls_nr, 'members' if cls_nr > 1 else 'member')

    # euler characteristic
    print "Euler characteristic:"
    for (i, c) in enumerate(components_copy):
        print "Class", i+1, ":", h.euler_characteristic(c)

    plt.show()


if __name__ == "__main__":
    main()

