import random
class Point:
    def __init__(self, x, y, point_number):
        self.x = float(x)
        self.y = float(y)
        self.point_number = point_number

    def __str__(self):
        #return "(%.4f, %.4f, %d)" % (self.x, self.y, self.point_number)
        return "%d" % (self.point_number)

    def __repr__(self):
        return self.__str__()

    def point_as_tuple(self):
        return self.x, self.y

    #in place
    def shake(self, shake_magnitude=0.1):
        delta_x = random.random()*2*shake_magnitude - shake_magnitude
        delta_y = random.random()*2*shake_magnitude - shake_magnitude
        self.x += delta_x
        self.y += delta_y


    @staticmethod
    def shake(point, shake_magnitude=0.1):
        delta_x = random.random()*2*shake_magnitude - shake_magnitude
        delta_y = random.random()*2*shake_magnitude - shake_magnitude
        return Point(point.x + delta_x, point.y + delta_y)