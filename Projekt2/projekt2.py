import dionysus
import itertools
from dionysus import PairwiseDistances, Rips, Filtration, StaticPersistence, data_dim_cmp
from math import sqrt
import drawing as drw
import helpers as h


def main():
    s1_points = h.read_data("data/s1_20.out")

    # Alpha shapes barcode diagram
    radius = barcode_diagram_alpha_shapes(s1_points, get_optimal_r=True, do_plot_barcode=True)
    a = alpha(s1_points, radius)
    drw.draw_complex(s1_points, a, radius, "Alpha-shapes")

    # VR barcode diagram
    max_radius = 3
    max_dim = int(2)
    radius = barcode_diagram_rips(s1_points, max_dim, max_radius, get_optimal_r=True, do_plot_barcode=True)
    r = rips(s1_points, max_dim, 2 * radius)
    drw.draw_complex(s1_points, r, radius, "Vietoris Rips")

    # Perturbations
    print "Robustness at optimal R={0}".format(radius)
    a_unchanged = 0
    r_unchanged = 0
    for i in range(0, 100):
        s1_points = h.shake(s1_points, radius)
        a_b0, a_b1 = barcode_diagram_alpha_shapes(s1_points, radius, do_plot_barcode=False, print_to_console=False)
        r_b0, r_b1 = barcode_diagram_rips(s1_points, max_dim, max_radius, radius, do_plot_barcode=False, print_to_console=False)

        if a_b0 == 1 and a_b1 == 0:
            a_unchanged += 1
        if r_b0 == 1 and r_b1 == 0:
            r_unchanged += 1

    print "Alpha shapes:", a_unchanged
    print "Vietoris Rips:", r_unchanged

    # Perturbations with larger R
    for faktor in [25, 50, 75, 100]:
        f = 1 + float(faktor) / 100.0
        larger_radius = radius * f
        a_b0_orig, a_b1_orig = barcode_diagram_alpha_shapes(s1_points, larger_radius, do_plot_barcode=False, print_to_console=False)
        r_b0_orig, r_b1_orig = barcode_diagram_rips(s1_points, max_dim, max_radius, larger_radius, do_plot_barcode=False, print_to_console=False)
        print "Robustness at {0}% larger R={1}".format(faktor, larger_radius)
        a_unchanged = 0
        r_unchanged = 0
        for i in range(0, 100):
            s1_points = h.shake(s1_points, larger_radius)
            a_b0, a_b1 = barcode_diagram_alpha_shapes(s1_points, larger_radius, do_plot_barcode=False, print_to_console=False)
            r_b0, r_b1 = barcode_diagram_rips(s1_points, max_dim, max_radius, larger_radius, do_plot_barcode=False, print_to_console=False)

            if a_b0 == a_b0_orig and a_b1 == a_b1_orig:
                a_unchanged += 1
            if r_b0 == r_b0_orig and r_b1 == r_b1_orig:
                r_unchanged += 1

        print "Alpha shapes:", a_unchanged
        print "Vietoris Rips:", r_unchanged

    bp = 0


def rips(points, skeleton, max_distance):
    """
    Generate the Vietoris-Rips complex on the given set of points in 2D.
    Only simplexes up to dimension skeleton are computed.
    The max parameter denotes the distance cut-off value.
    """
    distances = dionysus.PairwiseDistances(points)
    rips = dionysus.Rips(distances)
    simplices = dionysus.Filtration()
    rips.generate(skeleton, max_distance, simplices.append)
    # print "Rips simplices:", len(simplices)
    for s in simplices: s.data = rips.eval(s)
    return [simplex for simplex in simplices]


def alpha(points, radius):
    f = dionysus.Filtration()
    dionysus.fill_alpha_complex(points, f)
    simplices = list(f)

    max_dist = radius * radius
    ret = [s for s in simplices if s.data[0] <= max_dist]
    # print "A-shapes simplices:", len(ret)
    return ret


def calculate_betti_0_1(dim0, dim1, radius):
    betti0 = sum(1 for x, y in dim0 if x <= radius <= y)
    betti1 = sum(1 for x, y in dim1 if x <= radius <= y)
    return betti0, betti1


def barcode_diagram_alpha_shapes(points, radius=0, get_optimal_r=True, do_plot_barcode=True, print_to_console=True):
    """
    http://www.mrzv.org/software/dionysus/examples/alphashape.html
    """
    f = dionysus.Filtration()
    dionysus.fill_alpha_complex(points, f)

    f.sort(data_dim_cmp)
    p = StaticPersistence(f)
    # p = DynamicPersistenceChains(f)
    p.pair_simplices()
    smap = p.make_simplex_map(f)

    dim0, dim1, dim2 = [], [], []
    infinity = 'inf'
    for i in p:
        if i.sign():
            b = smap[i]
            dimension = b.dimension()
            if i.unpaired():
                if dimension == 0:
                    dim0.append([sqrt(b.data[0]), infinity])
                elif dimension == 1:
                    dim1.append([sqrt(b.data[0]), infinity])
                elif dimension == 2:
                    dim2.append([sqrt(b.data[0]), infinity])
                continue

            d = smap[i.pair()]
            birth = b.data[0]
            death = d.data[0]
            if birth != death:
                if dimension == 0:
                    dim0.append([sqrt(b.data[0]), sqrt(d.data[0])])
                elif dimension == 1:
                    dim1.append([sqrt(b.data[0]), sqrt(d.data[0])])
                elif dimension == 2:
                    dim2.append([sqrt(b.data[0]), sqrt(d.data[0])])

    max_death = max(y for x, y in itertools.chain(dim0, dim1) if y is not infinity)
    inf_factor = max_death + 0.5

    for i in range(0, len(dim0)):
        if dim0[i][1] is infinity:
            dim0[i][1] = inf_factor

    for i in range(0, len(dim1)):
        if dim1[i][1] is infinity:
            dim1[i][1] = inf_factor

    min_r = 0

    if do_plot_barcode or get_optimal_r:
        for i in h.f_range(0.0, 3, 0.01):
            b0, b1 = calculate_betti_0_1(dim0, dim1, i)
            if b0 == 1 and b1 == 0:
                if print_to_console:
                    print("Minimal radius alpha shapes (network is connected without holes): %.2f" % i)
                min_r = i
                break
                # print("(r=%.2f)\tBetti 0 = %d\tBetti 1 = %d" % (i, b0, b1))

    if do_plot_barcode:
        title = 'Alpha shapes barcode diagram'
        if len(points[1]) == 2:
            drw.draw_barcode_diagram([dim0, dim1], title, (min_r if min_r > 0 else radius), inf_factor)

        elif len(points[1]) == 3:
            drw.draw_barcode_diagram([dim0, dim1, dim2], title, (min_r if min_r > 0 else radius), inf_factor)
        return min_r
    else:
        return calculate_betti_0_1(dim0, dim1, radius)


def barcode_diagram_rips(points, skeleton, max_distance, radius=0, get_optimal_r=True, do_plot_barcode=True, print_to_console=False):
    """
    http://www.mrzv.org/software/dionysus/examples/rips.html
    """
    distances = PairwiseDistances(points)
    # distances = ExplicitDistances(distances)           # speeds up generation of the Rips complex at the expense of memory usage
    rips = Rips(distances)

    simplices = Filtration()
    rips.generate(skeleton, max_distance, simplices.append)

    # While this step is unnecessary (Filtration below can be passed rips.cmp),
    # it greatly speeds up the running times
    for s in simplices: s.data = rips.eval(s)
    simplices.sort(data_dim_cmp)  # could be rips.cmp if s.data for s in simplices is not set

    p = StaticPersistence(simplices)

    p.pair_simplices()

    dim0, dim1, dim2 = [], [], []
    infinity = 'inf'
    smap = p.make_simplex_map(simplices)
    for i in p:
        if i.sign():
            b = smap[i]
            dimension = b.dimension()
            if dimension >= skeleton:
                continue

            b.data /= 2
            if i.unpaired():
                if dimension == 0:
                    dim0.append([b.data, infinity])
                elif dimension == 1:
                    dim1.append([b.data, infinity])
                elif dimension == 2:
                    dim2.append([b.data, infinity])
                continue

            d = smap[i.pair()]
            d.data /= 2
            birth = b.data
            death = d.data
            if birth != death:
                if dimension == 0:
                    dim0.append([b.data, d.data])
                elif dimension == 1:
                    dim1.append([b.data, d.data])
                elif dimension == 2:
                    dim2.append([b.data, d.data])

    max_death = max(y for x, y in itertools.chain(dim0, dim1) if y is not infinity)
    inf_factor = max_death + 0.5

    for i in range(0, len(dim0)):
        if dim0[i][1] is infinity:
            dim0[i][1] = inf_factor

    for i in range(0, len(dim1)):
        if dim1[i][1] is infinity:
            dim1[i][1] = inf_factor

    min_r = 0

    if do_plot_barcode or get_optimal_r:
        for i in h.f_range(0.0, 3, 0.01):
            b0, b1 = calculate_betti_0_1(dim0, dim1, i)
            if b0 == 1 and b1 == 0:
                if print_to_console:
                    print("Minimal radius VR (network is connecetd without holes): %.2f" % i)
                min_r = i
                break
                # print("(r=%.2f)\tBetti 0 = %d\tBetti 1 = %d" % (i, b0, b1))

    if do_plot_barcode:
        title = 'Vietoris Rips barcode diagram'
        if len(points[1]) == 2:
            drw.draw_barcode_diagram([dim0, dim1], title, (min_r if min_r > 0 else radius), inf_factor)

        elif len(points[1]) == 3:
            drw.draw_barcode_diagram([dim0, dim1, dim2], title, (min_r if min_r > 0 else radius), inf_factor)

        return min_r
    else:
        return calculate_betti_0_1(dim0, dim1, radius)


if __name__ == "__main__":
    main()
