import numpy as np
from collections import defaultdict
from Simplex import Simplex
from Complex import Complex
from Point import Point


class Cech:
    def __init__(self, points, r=0.1, measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1)-np.array(p2), 2)))):
        '''
        :param points: seznam tock :[Point(x1,y1), Point(x2,y2),...]
        :param r:  float polmer radija celice
        :param measure: razdalja med tockama
        :return:
        '''
        if len(points) == 0:
            raise "No points"
        self.points = points
        self.r = r
        self.measure = measure

    def __call__(self, r=None, measure=None):
        self.r = r if r else self.r
        self.measure = measure if measure else self.measure
        return self.cech()


    def cech(self):
        n = len(self.points)
        cech_comp = set(self.points)
        edges = []
        edges_set = []
        sosedi = defaultdict(list)
        for i in range(n):
            point1 = self.points[i]
            sosedi[point1] = []
            for j in range(i+1, n):
                point2 = self.points[j]
                distance = self.measure(point1.point_as_tuple(), point2.point_as_tuple())
                if distance <= 2* self.r:
                    sosedi[point1].append(point2)
                    edges_set.append(set([point1,point2]))
                    edges.append((point1, point2))
                    #cech_comp.discard(point1)
                    #cech_comp.discard(point2)

        #trikotniki
        m = len(edges)
        trikotniki = []

        for i in range(m):
            point1 = edges[i][0]
            point2 = edges[i][1]

            # kandidati za trikotnike
            sosedi1 = sosedi[point1]
            sosedi2 = sosedi[point2]

            kandidati = [val for val in sosedi1 if val in sosedi2] # presek sosedov

            pres1, pres2 = self._presecisceKroznic(point1,self.r, point2, self.r)

            for j in range(len(kandidati)):
                point3 = kandidati[j]
                d1 = self.measure(pres1.point_as_tuple(), point3.point_as_tuple())
                d2 = self.measure(pres2.point_as_tuple(), point3.point_as_tuple())

                # ce je katero izmd presecisc znotraj radija, potem tvorijo tocke trikotnik
                if (d1<=self.r or d2<=self.r):
                    trikotniki.append((point1, point2, point3))

        daljice = []
        for t in trikotniki:
            tri = list(t)
            set1 = set([tri[0], tri[1]])
            set2 = set([tri[0], tri[2]])
            set3 = set([tri[1], tri[2]])

            if set1 in trikotniki:
                ix = edges_set.index(set1)
                edges_set.pop(ix)
            else:
                pass#daljice.append(set1)
            if set2 in trikotniki:
                ix = edges_set.index(set2)
                edges_set.pop(ix)
            else:
                pass#daljice.append(set2)
            if set3 in trikotniki:
                ix = edges_set.index(set3)
                edges_set.pop(ix)
            else:
                pass#daljice.append(set3)

        res = []
        for p in cech_comp:
            tmp = (p,)
            #tmp.add(p)
            res.append(tmp)
        for d in edges:
            res.append(d)
        for t in trikotniki:
            res.append(t)

        rezultat = []
        while (len(res)>0):
            komplex = set()
            sx = res.pop()
            komplex.add(sx)

            #print('komplex', komplex)
            j = 0
            sxi = list(sx)
            if len(sxi)<=1:
                # na tem mestu smo vse 1-simplekse ze porabili
                pass
            else:
                while j<len(sxi) and len(sxi)>1:
                    el = sxi[j]
                    i = 0
                    while (i<len(res)):
                        sx1 = res[i]
                        if el in sx1:
                            res.pop(i)
                            komplex.add(sx1)
                            temp = [e for e in sx1 if (e not in sxi and len(sx1)>1)]
                            sxi = sxi + temp
                        else:
                            i+=1
                    j+=1
            rezultat.append(komplex.copy())

        #print('ceh',rezultat)

        chains = []
        for chain in rezultat:
            chain_s = [Simplex(tuple(simplex)) for simplex in chain]
            chains.append(chain_s)
        return Complex(chains, self.r)


    def _presecisceKroznic(self, p1, r1, p2, r2):
        '''
        Izracuna presecisci kroznic s srediscem v p1 in polmerom r1 ter srediscem v p2 ter polmerom r2
        :param p1: sredisce prve kroznice : Point(x1,y1)
        :param r1: float polmer prve kroznice
        :param p2: ...
        :param r2: ...
        :return: vrne (Point(x1, y1), Point(x2,y2)); tocki preseka

        '''
        a,b = p1.point_as_tuple()
        c,d = p2.point_as_tuple()

        e = c - a
        f = d - b
        p = np.sqrt(e*e + f*f)
        k = (p*p + r1*r1 - r2*r2) / (2*p)

        x1 = a + e*k/p + (f/p) * np.sqrt(r1*r1 - k*k)
        y1 = b + f*k/p - (e/p) * np.sqrt(r1*r1 - k*k)

        x2 = a + e*k/p - (f/p) * np.sqrt(r1*r1 - k*k)
        y2 = b + f*k/p + (e/p) * np.sqrt(r1*r1 - k*k)

        return (Point(x1,y1,-1), Point(x2,y2,-1))

