# from dionysus import Simplex, Filtration, StaticPersistence, vertex_cmp, data_cmp, data_dim_cmp, \
#     DynamicPersistenceChains
#
# # votel trikotnik
# complex = [Simplex((0,), 0),  # A
#            Simplex((1,), 0),  # B
#            Simplex((2,), 0),  # C
#            Simplex((0, 1), 0),  # AB
#            Simplex((1, 2), 0),  # BC
#            Simplex((0, 2), 0),  # CA
#            ]
#
# f = Filtration(complex, data_cmp)
# p = DynamicPersistenceChains(f)
# p.pair_simplices()
# smap = p.make_simplex_map(f)
#
# for i in [i for i in p if i.unpaired()]:
#     print "Dim: {0}: {1}".format(smap[i].dimension(),
#                                  [smap[ii] for ii in i.chain])

tmp = [[0, 1], [0, 2], [2, 4], [4, 6], [4, 5],
       [7, 8], [8, 9], [8, 10], [7, 10],
       [11, 12]]

classes = []

clss = 0


def find_connected_simplices(current, tmp):
    [x, y] = current
    new_tmp = [x, y]

    for x_tmp, y_tmp in tmp:
        if x_tmp == x or x_tmp == y or y_tmp == x or y_tmp == y:
            new_tmp.append()

    return new_tmp


while tmp:
    clss += 1
    current = tmp.pop()
    classes.append([current, clss])

    indexes_set = find_connected_simplices(current, tmp)

print classes
# tmp.remove()
