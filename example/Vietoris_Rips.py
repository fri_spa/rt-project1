import numpy as np
import math
from collections import defaultdict

#measure=lambda p1, p2: math.sqrt(sum([(p1[i]-p2[i])**2 for i in range(len(p1))]))
def main(points, r, measure=lambda p1, p2: np.sqrt(np.sum(np.power(np.array(p1)-np.array(p2), 2)))):
    """ r - radius
        measure - distance measure function
                  function accepts two points, each represented as numpy array
                  it returns distance between points
                  deafult: euclidian distance
    """
    VR_complex = set(points)

    points_by_distance = defaultdict(list)
    connected_points = []

    for i in range(len(points)):
        point1 = points[i]
        for j in range(i+1, len(points)):
            point2 = points[j]
            distance = measure(point1, point2)
            if distance <= 2*r:
                points_by_distance[distance].append(set([point1, point2]))
                connected_points.append(set([point1, point2]))
                VR_complex.discard(point1)
                VR_complex.discard(point2)

    temp = []
    for sx in VR_complex:
        tmp = set()
        tmp.add(sx)
        temp.append(tmp)
    VR_complex = temp

    while(True):
        for i, e1 in enumerate(connected_points):
            if sum([e1 <= s for s in VR_complex]) > 0:
                continue
            sx = e1
            for j in range(i+1, len(connected_points)):
                e2 = connected_points[j]
                if len(sx & e2) > 0:
                    sx.update(sx | e2)
            VR_complex.append(sx)
        if connected_points == VR_complex:
            break
        else:
            connected_points = VR_complex
            VR_complex = []
    return VR_complex